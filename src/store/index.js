import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        deviceID: 0,
        isLogin: false,
    },
    mutations: {
        changeDeviceID: function (state, deviceID) {
            state.deviceID = deviceID;
            localStorage.deviceID = deviceID
        },
        changeIsLogin: function (state, isLogin) {
            state.isLogin = isLogin;
            localStorage.isLogin = isLogin
        },
    },
    actions: {},
    modules: {}
})
